#include "..\include\framework.h"
#include "..\include\af-windows.h"
#include "..\include\af-windows-scan.h"

#include "..\include\debug.h"

static int createAppKey(PHKEY phHandle);
static int recreateResolutionKey(HKEY hRegRoot, PHKEY phResolutionRegKey, PCHAR pszResolution);
static void calculateResolution(PCHAR pszRezolution, size_t maxLen);
static void adjustWindow(HWND hwnd, char* pszPosition);

static BOOL CALLBACK EnumWindowsProc(_In_ HWND   hwnd, _In_ LPARAM lParam);
static BOOL CALLBACK EnumWindowsProc2(_In_ HWND   hwnd, _In_ LPARAM lParam);

static int nSuspensionCounter = 0, bSuspensionPeriod = 0;

/*
long styles[] = {
	WS_OVERLAPPED       ,
	WS_POPUP            ,
	WS_CHILD            ,
	WS_MINIMIZE         ,
	WS_VISIBLE          ,
	WS_DISABLED         ,
	WS_CLIPSIBLINGS     ,
	WS_CLIPCHILDREN     ,
	WS_MAXIMIZE         ,
	WS_CAPTION          ,
	WS_BORDER           ,
	WS_DLGFRAME         ,
	WS_VSCROLL          ,
	WS_HSCROLL          ,
	WS_SYSMENU          ,
	WS_THICKFRAME       ,
	WS_GROUP            ,
	WS_TABSTOP          ,

	WS_MINIMIZEBOX      ,
	WS_MAXIMIZEBOX      ,
};

char styleNames[][20] = {
	"WS_OVERLAPPED",
	"WS_POPUP",
	"WS_CHILD",
	"WS_MINIMIZE",
	"WS_VISIBLE",
	"WS_DISABLED",
	"WS_CLIPSIBLINGS",
	"WS_CLIPCHILDREN",
	"WS_MAXIMIZE",
	"WS_CAPTION",
	"WS_BORDER",
	"WS_DLGFRAME",
	"WS_VSCROLL",
	"WS_HSCROLL",
	"WS_SYSMENU",
	"WS_THICKFRAME",
	"WS_GROUP",
	"WS_TABSTOP",

	"WS_MINIMIZEBOX",
	"WS_MAXIMIZEBOX"
};
*/

struct ResolutionsAndHandles {

	int nAllAdjustedHandels[1024], nAllAdjustedHandelsIndex = 0;
	PCHAR pszResolution;
};

static HKEY hRegRoot = 0;

void af_closeAllRegistry() {

	if (hRegRoot != 0)
		RegCloseKey(hRegRoot);
}

void af_scanWindows() {

	DEB("Scanning windows");

	createAppKey(&hRegRoot);

	CHAR szResolution[25] = {0};
	calculateResolution(szResolution, sizeof(szResolution));

	DEB1("Current resolution %s", szResolution);

	CHAR szLastResolution[256] = {0};
	DWORD dwSize = sizeof(szLastResolution);
	if (RegGetValueA(hRegRoot, NULL, "LastResolution", RRF_RT_REG_SZ, NULL, szLastResolution, &dwSize) != ERROR_SUCCESS)
		DEB("Error reading LastResolution registry value");
	else
		DEB1("LastResolution resolution %s", szLastResolution);

	if (strcmp(szResolution, szLastResolution) != 0) {

		DEB2("Resolution is changed from (%s) to (%s)", szLastResolution, szResolution);
		if (!bSuspensionPeriod) {
			bSuspensionPeriod = 1;
			nSuspensionCounter = (SCAN_SUSPEND_ON_RES_CHANGE_INTERVAL_SEC * 1000) / SCAN_INTERVAL_MS;
		}

		ResolutionsAndHandles rah;
		rah.nAllAdjustedHandelsIndex = 0;
		rah.pszResolution = szResolution;
		
		HKEY hExistingResolutionRegKey;
		if (RegOpenKeyA(hRegRoot, szResolution, &hExistingResolutionRegKey) == ERROR_SUCCESS) {

			DEB1("Previous data exists for the resolution %s", szResolution);
			
			CHAR szPosition[256], szHandle[20];
			DWORD dwUnused = sizeof(szHandle), dwUnused2 = sizeof(szPosition);
			int rc, nIndex = 0;

			while (true) {

				memset(szPosition, 0, sizeof(szPosition));
				memset(szHandle, 0, sizeof(szHandle));
				dwUnused = sizeof(szHandle), dwUnused2 = sizeof(szPosition);
				rc = RegEnumValueA(hExistingResolutionRegKey, nIndex, szHandle, &dwUnused, NULL, NULL, (LPBYTE)szPosition, &dwUnused2);

				DEB2("Existing data for %s %s", szHandle, szPosition);

				nIndex++;

				if (rc != ERROR_SUCCESS)
					break;

				if (strstr(szHandle, "-t") == 0) {

					HWND hwnd = 0;
					sscanf(szHandle, "%d", (unsigned*)& hwnd);

					adjustWindow(hwnd, szPosition);
					sscanf(szHandle, "%d", &rah.nAllAdjustedHandels[rah.nAllAdjustedHandelsIndex ++]);
				}
			}

			RegCloseKey(hExistingResolutionRegKey);
		}

		DEB1("Adjusted handles %d", rah.nAllAdjustedHandelsIndex);
		EnumWindows(EnumWindowsProc2, (LPARAM)&rah);
	}

	if (bSuspensionPeriod) {

		nSuspensionCounter--;
		if (nSuspensionCounter <= 0)
			bSuspensionPeriod = 0;

		DEB2("Decreased resolution change suspension counter: %d period: %d", nSuspensionCounter, bSuspensionPeriod);
	}

	if (!bSuspensionPeriod) {

		if (RegSetKeyValueA(hRegRoot, NULL, "LastResolution", REG_SZ, szResolution, sizeof(szResolution) + 1) != ERROR_SUCCESS)
			ERR("Error creating \"LastResolution\" registry value");

		HKEY hResolutionRegKey;
		recreateResolutionKey(hRegRoot, &hResolutionRegKey, szResolution);

		EnumWindows(EnumWindowsProc, (LPARAM)hResolutionRegKey);

		DEB("Closing registry keys");
		RegCloseKey(hResolutionRegKey);
	} 

	RegCloseKey(hRegRoot);
}

static void adjustWindow(HWND hwnd, char* pszPosition) {

	WINDOWPLACEMENT placement;
	memset(&placement, 0, sizeof(WINDOWPLACEMENT));
	placement.length = sizeof(WINDOWPLACEMENT);

	int nMazimizedFlag;

	sscanf(pszPosition, "%d %d %d %d %d", &(placement.rcNormalPosition.top), &(placement.rcNormalPosition.right), &(placement.rcNormalPosition.bottom), &(placement.rcNormalPosition.left), &nMazimizedFlag);


	if (nMazimizedFlag == WPF_RESTORETOMAXIMIZED)
		ShowWindow(hwnd, SW_MAXIMIZE);
	else {

		ShowWindow(hwnd, SW_NORMAL);
		if (SetWindowPos(hwnd, HWND_TOP, placement.rcNormalPosition.left, placement.rcNormalPosition.top, placement.rcNormalPosition.right - placement.rcNormalPosition.left, placement.rcNormalPosition.bottom - placement.rcNormalPosition.top, SWP_NOZORDER) == 0)
			ERR("Error placing window");
	}

}

static int recreateResolutionKey(HKEY hRegRoot, PHKEY phResolutionRegKey, PCHAR pszResolution) {


	CHAR szRezolutionRegValue[25];
	snprintf(szRezolutionRegValue, sizeof(szRezolutionRegValue), "%s", pszResolution);
	
	int rc = RegDeleteKeyA(hRegRoot, szRezolutionRegValue);
	if (rc != ERROR_SUCCESS && rc != 0x2) {

		ERR1("Error recreating resolution key unable to delete the old one (%s)", szRezolutionRegValue);
		return rc;
	}

	rc = RegCreateKeyA(hRegRoot, szRezolutionRegValue, phResolutionRegKey);
	if (rc != ERROR_SUCCESS) {

ERR1("Error recreating resolution key unable to create the (%s)", szRezolutionRegValue);
return rc;
	}

	return 0;
}

static void calculateResolution(PCHAR pszRezolution, size_t maxLen) {

	snprintf(pszRezolution, maxLen, "%dx%dx%d", GetSystemMetrics(SM_CMONITORS), GetSystemMetrics(SM_CXVIRTUALSCREEN), GetSystemMetrics(SM_CYVIRTUALSCREEN));
}


static int createAppKey(PHKEY phHandle) {

	HKEY hSoftwareKey;
	int rc = RegOpenKeyA(HKEY_CURRENT_USER, "SOFTWARE", &hSoftwareKey);

	if (rc != ERROR_SUCCESS) {

		ERR("Error creating application registry key, open of HKEY_CURRENT_USER\\SOFTWARE is failed");
		return rc;
	}

	rc = RegCreateKeyA(hSoftwareKey, "AF", phHandle);

	if (rc != ERROR_SUCCESS) {

		ERR("Error creating application registry key");
		return rc;
	}

	return rc;
}

BOOL CALLBACK EnumWindowsProc(_In_ HWND   hwnd, _In_ LPARAM hResolutionRegKey) {

	int nWinLong = GetWindowLongA(hwnd, GWL_STYLE);

	if ((nWinLong & WS_BORDER) == 0 || (nWinLong & WS_VISIBLE) == 0 || (nWinLong & WS_POPUP) != 0)
		return TRUE;

	CHAR szTitle[256];
	int rc = GetWindowTextA(hwnd, szTitle, sizeof(szTitle));
	if (rc == 0)
		snprintf(szTitle, sizeof(szTitle), "no title");

	CHAR szClassName[256];
	rc = GetClassNameA(hwnd, szClassName, sizeof(szClassName));
	DEB1("Window class name %s", szClassName);

	//	DEB3("Found windows %s with handle 0x%x, 0x%x", szTitle, hwnd, nWinLong);

	WINDOWPLACEMENT placement;
	rc = GetWindowPlacement(hwnd, &placement);
	if (rc == 0) {
		ERR1("Error in GetWindowPlacement for 0x%x", hwnd);
		return TRUE;
	}

	CHAR szPosition[256], szHandle[20];
	snprintf(szPosition, sizeof(szPosition), "%d %d %d %d %d", placement.rcNormalPosition.top, placement.rcNormalPosition.right, placement.rcNormalPosition.bottom, placement.rcNormalPosition.left, placement.flags);
	snprintf(szHandle, sizeof(szHandle), "%d", (unsigned)hwnd);

	DEB3("Recording: handle %s, position: %s, title: %s", szHandle, szPosition, szTitle);

	if (RegSetKeyValueA((HKEY)hResolutionRegKey, NULL, szHandle, REG_SZ, szPosition, strlen(szPosition) + 1) != ERROR_SUCCESS)
		ERR3("Error creating registry value (%s) for window (%s) title (%s)", szPosition, szHandle, szTitle);

	snprintf(szHandle, sizeof(szHandle), "%d-t", (unsigned)hwnd);
	if (RegSetKeyValueA((HKEY)hResolutionRegKey, NULL, szHandle, REG_SZ, szClassName, strlen(szClassName) + 1) != ERROR_SUCCESS)
		ERR3("Error creating registry value (%s) for window (%s) title (%s)", szPosition, szHandle, szTitle);

	snprintf(szHandle, sizeof(szHandle), "%d-tt", (unsigned)hwnd);
	if (RegSetKeyValueA((HKEY)hResolutionRegKey, NULL, szHandle, REG_SZ, szTitle, strlen(szTitle) + 1) != ERROR_SUCCESS)
		ERR3("Error creating registry value (%s) for window (%s) title (%s)", szPosition, szHandle, szTitle);

	return TRUE;
}

BOOL CALLBACK EnumWindowsProc2(_In_ HWND   hwnd, _In_ LPARAM lparam) {

	ResolutionsAndHandles* pHar = (ResolutionsAndHandles*)lparam;

	int nWinLong = GetWindowLongA(hwnd, GWL_STYLE);

	if ((nWinLong & WS_BORDER) == 0 || (nWinLong & WS_VISIBLE) == 0 || (nWinLong & WS_POPUP) != 0)
		return TRUE;

	int bFound = false;
	for (int i = 0; i < pHar->nAllAdjustedHandelsIndex; i++) {

		if (pHar->nAllAdjustedHandels[i] == (int)hwnd) {
			bFound = true;
			break;
		}
	}

	if (!bFound) {

		DEB1("Window with handle %d is new", hwnd);

		HKEY hPresetsKey;
		if (RegOpenKeyA(hRegRoot, "Presets", &hPresetsKey) == ERROR_SUCCESS) {

			HKEY hResolutionKey;
			if (RegOpenKeyA(hPresetsKey, pHar->pszResolution, &hResolutionKey) == ERROR_SUCCESS) {

				CHAR szClassName[256];
				int rc = GetClassNameA(hwnd, szClassName, sizeof(szClassName));
				DEB1("Window class name [%s]", szClassName);

				CHAR szPosition[256];
				DWORD dwSize = sizeof(szPosition);
				if (RegGetValueA(hResolutionKey, NULL, szClassName, RRF_RT_REG_SZ, NULL, szPosition, &dwSize) == ERROR_SUCCESS) {

					DEB2("Adjusting window %s to position %s", szClassName, szPosition);
					adjustWindow(hwnd, szPosition);
				}
				else
					DEB2("No predefined position found for class [%s] resolution [%s] %d", szClassName, pHar->pszResolution);

				RegCloseKey(hResolutionKey);
			}
			else
				DEB1("No predefined position found for resoluition %s", pHar->pszResolution);

			RegCloseKey(hPresetsKey);
		}
		else
			DEB("No presets found");
	}
	


	return TRUE;
}