#include "..\include\framework.h"
#include "..\include\af-windows.h"

#include "..\include\debug.h"
#include "..\include\tray-icon.h"
#include "..\include\main-window.h"
#include "..\include\af-windows-scan.h"


extern HINSTANCE g_hInstance;


static BOOL af_manipulateTrayIcon(HWND hMainWnd, DWORD dwMessage, HICON hIcon, LPCWSTR pszTip);

void af_onTrayIconClick(HWND hMainWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (lParam )
    {
        case WM_RBUTTONUP:
		  break;

        case WM_LBUTTONDBLCLK:
			af_closeAllRegistry();
			PostQuitMessage(0);
            break;
    }

	
}

int af_createTrayIcon(HWND hMainWnd)
{
	af_manipulateTrayIcon(hMainWnd, NIM_ADD, NULL, NULL);

	HICON hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_SMALL));
	if (!hIcon)
	{
		ERR("Can't load icon image");
		return - 1;
	}

	af_manipulateTrayIcon(hMainWnd, NIM_MODIFY, hIcon, L"af-windows");
        
	return 0;
}

static BOOL af_manipulateTrayIcon(HWND hMainWnd, DWORD dwMessage, HICON hIcon, LPCWSTR pszTip)
{
	DEB2("af_manipulateTrayIcon 0x%x, 0x%p", dwMessage, hIcon);

    NOTIFYICONDATA tnd;
    tnd.cbSize				= sizeof(NOTIFYICONDATA);
    tnd.hWnd				= hMainWnd;
    tnd.uID					= af_TrayIconId;
    tnd.uFlags				= NIF_MESSAGE|NIF_ICON|NIF_TIP;
	tnd.uCallbackMessage	= af_TrayIconEvent;
    tnd.hIcon				= hIcon;

    lstrcpyn(tnd.szTip, pszTip, sizeof(tnd.szTip));

    BOOL res = Shell_NotifyIcon(dwMessage, &tnd);

    if (hIcon)
        DestroyIcon(hIcon);

    return res;
}