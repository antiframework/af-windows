// af-windows.cpp : Defines the entry point for the application.
//

#include "..\include\framework.h"
#include "..\include\af-windows.h"

#include "..\include\debug.h"
#include "..\include\main-window.h"

static bool af_AnotherInstanceRunning();
HINSTANCE g_hInstance;

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPTSTR lpCmdLine, _In_ int nCmdShow)
{
	DEB1("Starting af-windows 0x%x...", hInstance);

	g_hInstance = hInstance;

	if (af_AnotherInstanceRunning())
	{
		DEB("Another instance of the application is running, exiting");

		return 0;
	}

	return af_CreateMainWindow(hInstance);
}

static bool af_AnotherInstanceRunning()
{
	if (CreateMutex(NULL, FALSE, L"af-windowsInstanceRunningCheckMutex") == 0)
	{
		ERR("Error creating mutex");
		return true;
	}

	if (GetLastError() == ERROR_ALREADY_EXISTS)
		return true;

	return false;
}