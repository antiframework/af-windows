
#include "..\include\framework.h"
#include "..\include\af-windows.h"

VOID debugW(const wchar_t *format, ...)
{
	va_list ap;
	va_start(ap, format);

	wchar_t buf[1024 * 4];
	_vsnwprintf(buf, sizeof(buf) / sizeof(wchar_t), format, ap);

	OutputDebugStringW(buf);
}

VOID debugA(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	char buf[1024 * 4];
	_vsnprintf(buf, sizeof(buf), format, ap);

	OutputDebugStringA(buf);
}
