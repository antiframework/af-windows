#include "..\include\framework.h"
#include "..\include\af-windows.h"

#include "..\include\debug.h"
#include "..\include\main-window.h"
#include "..\include\tray-icon.h"
#include "..\include\af-windows-scan.h"

static const wchar_t mainWindowClassName[] = L"AFWindowsMainWindowClassName";

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Forward declarations
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static ATOM af_RegisterMainClass(HINSTANCE hInstance);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Public
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
DWORD af_CreateMainWindow(HINSTANCE hInstance)
{
	if (af_RegisterMainClass(hInstance) == 0)
	{
		ERR("Unable to register class for main window");

		return -1;
	}

	HWND hMainWnd = CreateWindow(mainWindowClassName, L"", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hMainWnd) {

		ERR("Unable to create main window");
		return -1;
	}

	UpdateWindow(hMainWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {	
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	UnregisterClass(mainWindowClassName, hInstance);
	
	DEB("Closing application");

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	WindProc
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static LRESULT CALLBACK af_MainWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//DEB1(L"pH_MainWindowProc with message 0x%x", message);

	switch (message)
	{
	case WM_CREATE:
		if (af_createTrayIcon(hWnd) != 0) 
			PostQuitMessage(0);

		SetTimer(hWnd, 1, SCAN_INTERVAL_MS, (TIMERPROC)NULL);
		break;
	case af_TrayIconEvent:
		af_onTrayIconClick(hWnd, message, wParam, lParam);
		break;
	case WM_TIMER: 

		if (wParam == 1) {

			DEB("Timer !!!");
			KillTimer(hWnd, 1);

			af_scanWindows();

			SetTimer(hWnd, 1, SCAN_INTERVAL_MS, (TIMERPROC)NULL);
		}

		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	private
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static ATOM af_RegisterMainClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)af_MainWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= NULL;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= mainWindowClassName;
	wcex.hIconSm		= NULL;

	return RegisterClassEx(&wcex);
}