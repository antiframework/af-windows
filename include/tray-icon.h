#pragma once

#include "..\include\framework.h"
#include "..\include\af-windows.h"

#include "..\include\debug.h"

#define af_TrayIconId 1000

int af_createTrayIcon(HWND hMainWnd);
void af_onTrayIconClick(HWND hMainWnd, UINT message, WPARAM wParam, LPARAM lParam);

