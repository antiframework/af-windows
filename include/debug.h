#pragma once

void debugW(const wchar_t *format, ...);
void debugA(const char *format, ...);

#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)
#define __WFILE__ WIDEN(__FILE__)

//#define DEBUG 1

#ifdef DEBUG 

#define DEB(message) debugA("af-windows debug %s:(%d) "##message, __FILE__, __LINE__)
#define DEB1(message, p1) debugA("af-windows debug %s:(%d) "##message, __FILE__, __LINE__, p1)
#define DEB2(message, p1, p2) debugA("af-windows debug %s:(%d) "##message, __FILE__, __LINE__, p1, p2)
#define DEB3(message, p1, p2, p3) debugA("af-windows debug %s:(%d) "##message, __FILE__, __LINE__, p1, p2, p3)
#define DEB4(message, p1, p2, p3, p4) debugA("af-windows debug %s:(%d) "##message, __FILE__, __LINE__, p1, p2, p3, p4)

#endif 

#ifndef DEBUG 

#define DEB(message) 
#define DEB1(message, p1)
#define DEB2(message, p1, p2)
#define DEB3(message, p1, p2, p3)
#define DEB4(message, p1, p2, p3, p4)


#endif 

#define ERR(message) debugA("af-windows error %s:(%d) "##message##" (%s)(0x%x)", __FILE__, __LINE__, errno, GetLastError())
#define ERR1(message, p1) debugA("af-windows error %s:(%d) "##message##" (%s)(0x%x)", __FILE__, __LINE__, p1, errno, GetLastError())
#define ERR2(message, p1, p2) debugA("af-windows error %s:(%d) "##message##" (%s)(0x%x)", __FILE__, __LINE__, p1, p2, errno, GetLastError())
#define ERR3(message, p1, p2, p3) debugA("af-windows error %s:(%d) "##message##" (%s)(0x%x)", __FILE__, __LINE__, p1, p2, p3, errno, GetLastError())
